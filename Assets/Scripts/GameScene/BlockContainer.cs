﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockContainer : MonoSingleton<BlockContainer>
{
    private const float DISTANCE_BETWEEN_BLOCKS = 0.38f;

    public GameObject rowPrefab;
    public Transform rowContainer;

    private float currentSpawnY;
    private Vector2 rowContainerStartingPosition;
    private Vector2 desiredPosition;

    private int lastBallSpawn;

    private List<Block> blocks = new List<Block>();

    private void Start()
    {
        rowContainerStartingPosition = rowContainer.transform.position;
        desiredPosition = rowContainerStartingPosition;
    }

    private void Update()
    {
        if ((Vector2)rowContainer.position != desiredPosition)
            rowContainer.transform.position = Vector3.MoveTowards(rowContainer.transform.position, desiredPosition, Time.deltaTime);
    }

    public void GenerateNewRow()
    {
        GameObject go = Instantiate(rowPrefab) as GameObject;
        go.transform.SetParent(rowContainer);
        go.transform.localPosition = Vector2.down * currentSpawnY;
        currentSpawnY -= DISTANCE_BETWEEN_BLOCKS;

        desiredPosition = rowContainerStartingPosition + (Vector2.up * currentSpawnY);

        Block[] blockArray = go.GetComponentsInChildren<Block>();

        int ballSpawnIndex = -1;
        if (lastBallSpawn * 0.3f > 1)
        {
            // Force spawn a ball
            ballSpawnIndex = Random.Range(0, 7);
            lastBallSpawn = 0;
        }

        for (int i = 0; i < blockArray.Length; i++)
        {
            if (ballSpawnIndex == i)
            {
                blockArray[i].SpawnBall();
                return;
            }

            if (Random.Range(0f, 1f) > 0.5f)
                blockArray[i].Spawn();
            else
                blockArray[i].Hide();
        }
        lastBallSpawn++;
    }
}
